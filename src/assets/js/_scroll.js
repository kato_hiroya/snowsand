// インスタンス : pageScroll
// <全ページ共通> スクロール制御
//=============================================================

export class Scroll {

  constructor() {

    this.enable = (PC || __WW__ > tabletStyle) ? true : false;
    this.ease = (__DETECT__.os.win) ? 0.9 : 0.2;
    this.history = { y: 0, dir: 'down' };
    this.blank = __WH__ / 20; // セクション変更ラインまでのブラウザトップからの距離
    this.blankRv = (__WH__ - this.blank); // ボトムからの距離

    this.createSectionArray();
    if (!isTransition) {
      this.reset();
    }
    this.$measure = document.querySelector('.scroll-height');
    this.$body = $window.querySelector('.page-origin');
    this.onLoadContents();

  }

  reset() {

    if (PC || __WW__ > tabletStyle) {
      for (var i = 0; i < $window.length; i++) {
        $window[i].scrollTop = 0;
      }
    } else {
      $window.scrollTop = 0;
    }

    window.scrollTo(0, 0);
    this.y = 0;
    __WT__ = 0;

    $html.classList.remove('scroll-down');
    $html.classList.remove('scroll-start');

    if (isTransition) {
      this.onResize();
    }

  }

  listener(changes) {

    changes.forEach((e, i) => {

      const el = e.target;
      const isIntersecting = e.isIntersecting;

      if (isIntersecting) {
        el.dataset.visible = 1;
        el.classList.add('is-visible');
      } else {
        el.dataset.visible = 0;
        el.classList.remove('is-visible');
      }

    });

  }

  onLoadContents() {

    this.createItemArray();
    this.onResize();

  }

  getPageHeight() {

    if (!this.$body) return false;
    this.total_height = this.$body.clientHeight;
    this.$measure.style.height = this.total_height + 'px';
    this.getSectionSize();

  }

  onResize() {

    this.getPageHeight();
    this.getItemSize();

  }


  //================================


  createSectionArray() {

    this.$sectionList = $window.querySelectorAll('[data-sct]');
    this.sct = [];
    this.$sectionList.forEach((el, i) => {
      this.sct.push({
        el: el,
        name: el.dataset.sct,
        pos: null,
      });
    });

    this.sections = [...this.$sectionList];
    if (this.sections.length) {
      let options = {
        threshold: 0
      };
      this.sectionObserver = new IntersectionObserver(this.listener, options);
      this.sections.forEach((e, i) => {
        this.sectionObserver.observe(e);
      });
    }

  }
  getSectionSize() {

    this.sct.forEach((v, i) => {
      const rect = v.el.getBoundingClientRect();
      v.pos = {
        bottom: rect.bottom + this.y,
        height: rect.height,
        left: rect.left,
        right: rect.right,
        top: rect.top + this.y,
        width: rect.width,
        x: rect.x,
        y: rect.y + this.y
      };
    });

  }
  sectionUpdate() {

    this.sct.forEach((item, i) => {

      if (item.pos.top < (this.y + this.blank)) {
        this.section = item.name;
      }

    });

    if (this.y < 10) {
      $html.classList.remove('scroll-start');
    } else {
      $html.classList.add('scroll-start');
    }

    if (this.preSection !== this.section) {
      this.preSection = this.section;

      removeClass($body, 's-');
      $body.classList.add('s-' + this.section);

      if (this.section === 'news' ||
        this.section === 'package' ||
        this.section === 'features') {
        $body.classList.add('invert');
      }
      if (this.section === 'visual1' ||
        this.section === 'visual2' ||
        this.section === 'visual3' ||
        this.section === 'footer' ||
        this.section === 'stores' ||
        this.section === 'maker' ||
        this.section === 'products' ||
        this.section === 'about' ||
        this.section === 'concept' ||
        this.section === 'mv') {
        $body.classList.remove('invert');
      }
    }
    //

  }

  //================================


  createItemArray() {

    const register = (el, i, array) => {
      array.push({
        i: i,
        el: el,
        x: 0,
        y: 0,
        visible: false,
        rect: null,
        pow: {
          x: Number(el.dataset.powx),
          y: Number(el.dataset.powy),
        }
      });
    };
    //===
    this.items = [];
    this.$scrollItems = document.querySelectorAll('.js-scrollAll');
    this.$scrollItems.forEach((el, i) => {
      register(el, i, this.items);
    });
    this.parallax = [];
    this.$parallax = document.querySelectorAll('.js-parallax');
    this.$parallax.forEach((el, i) => {
      register(el, i, this.parallax);
    });
    //===
    this.targets = [...this.$scrollItems];
    if (this.targets.length) {
      let options = {
        threshold: 0
      };
      this.observer = new IntersectionObserver(this.listener, options);
      this.targets.forEach((e, i) => {
        this.observer.observe(e);
      });
    }

  }
  getItemSize() {

    this.items.forEach((v, i) => {
      const rect = v.el.getBoundingClientRect();
      v.rect = {
        bottom: rect.bottom + this.y,
        height: rect.height,
        left: rect.left,
        right: rect.right,
        top: rect.top + this.y,
        width: rect.width,
        x: rect.x,
        y: rect.y + this.y
      };
    });
    this.parallax.forEach((v, i) => {
      const rect = v.el.getBoundingClientRect();
      v.rect = {
        bottom: rect.bottom + this.y,
        height: rect.height,
        left: rect.left,
        right: rect.right,
        top: rect.top + this.y,
        width: rect.width,
        x: rect.x,
        y: rect.y + this.y
      };
    });

  }


  //================================


  domTransform() {

    this.items.forEach((v, i) => {

      const visible = parseInt(v.el.dataset.visible);
      const min = (v.rect.bottom) + (-this.y);
      const max = this.y + __WH__;
      const area = max - min;
      const ratio = (this.y - v.rect.top) / v.rect.height;

      if ((v.rect.top < max && min > 0) || visible === 1) {

        v.y = this.y;
        v.visible = true;
        v.el.style.transform = 'translate3d(' + v.x + 'px, ' + -v.y + 'px, 0)';

      } else {

        v.visible = false;

      }

    });
    //

  }
  parallaxTransform() {

    this.parallax.forEach((v, i) => {

      const _min = (v.rect.bottom) + (-this.y);
      const _max = this.y + __WH__;
      //
      if ((v.rect.top < _max && _min > 0)) {

        v.y = this.y;

        if (v.pow.y) {
          const _cy = ((v.y - v.rect.height / 2) - (v.rect.top - __WH__ / 2)) / __WH__;
          v.el.style.transform = 'translate3d(0px, ' + _cy * v.rect.height * v.pow.y + 'px, 0)';
        }

      } else {
      }

    });

  }

  updateDesktop() {

    this.onUpdate();
    this.domTransform();
    this.sectionUpdate();
    this.parallaxTransform();

  }

  updateMobile() {

    this.onUpdate();
    this.sectionUpdate();
    this.parallaxTransform();

  }

  onUpdate() {

    if (!this.enable) {
      this.y = __WT__;
    } else {
      this.y = this.easing(this.y, __WT__, this.ease);
    }

    this.getDirection();

  }

  getPercentage() {

    let __WTP__ = this.y / (this.total_height - __WH__);

  }
  easing(easeY, y, ease) {

    easeY += (y - easeY) * ease;
    if (0.001 >= easeY) easeY = 0;
    return easeY;

  }
  getDirection() {

    if (this.history.y < __WT__) {

      this.history.dir = 'down';
      $html.classList.add('scroll-down');
      $html.classList.remove('scroll-up');

    } else if (this.history.y > __WT__) {

      this.history.dir = 'up';
      $html.classList.add('scroll-up');
      $html.classList.remove('scroll-down');

    }

    this.history.y = __WT__;

  }

}

//=============================================================
