//
import isMobile from 'ismobilejs';
window.isMobile = isMobile;

//
import { gsap } from 'gsap';
import { ScrollToPlugin } from 'gsap/ScrollToPlugin';
window.gsap = gsap;
gsap.registerPlugin(ScrollToPlugin);

//
import barba from '@barba/core';
window.barba = barba;

//
import * as IntersectionObserver from 'intersection-observer';
