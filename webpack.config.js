/* ==================================================

./src/assetsに入れた画像を staticへコピーして 圧縮 + webp生成

================================================== */
const webpack = require('webpack');
const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const ImageminMozjpeg = require('imagemin-mozjpeg');
const imageminGifsicle = require('imagemin-gifsicle');
const ImageminWebpWebpackPlugin = require('imagemin-webp-webpack-plugin');

const srcpath = './src/assets/';
const outputpath = './src/static/assets/';

module.exports = {

  mode: 'production',
  target  : 'web',

  entry:  path.resolve(__dirname, 'webpack-entry.js'),
  output: {
    path: path.resolve(__dirname),
    filename: 'webpack-output.js' // 不要
  },

  plugins: [
    // Copy
    new CopyPlugin({
      patterns: [
        {
          from: srcpath + 'img',
          to: outputpath + 'img',
        },
        {
          from: srcpath + 'icons',
          to: outputpath + 'icons',
        },
      ],
    }),
    // Imagemin
    new ImageminPlugin({
      test: /\.(jpe?g|png|gif|svg|webp)$/i,
      pngquant: {
        quality: '80-90'
      },
      gifsicle: {
        interlaced: false,
        optimizationLevel: 3,
        colors: 256
      },
      svgo: {
      },
      plugins: [
        ImageminMozjpeg({
          quality: 85,
          progressive: true
        }),
      ]
    }),
    //Webp
    new ImageminWebpWebpackPlugin({
      config: [{
        test: /\.(jpe?g|png|gif)/,
        options: {
          quality: 85
        }
      }],
      overrideExtension: false, // image.png -> image.png.webp
    }),
  ],

}
